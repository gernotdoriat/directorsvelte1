import adapter from '@sveltejs/adapter-netlify';

const config = {
	kit: {
		adapter: adapter({
			split: false
		}),
		target: '#svelte'
	}
};

export default config;
