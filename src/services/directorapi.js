import { logE, logW, logI, logT } from "./log"

let urlDirectorApi = "https://api.inlume.io"
//let urlDirectorApi = "http://localhost:8000"
import { AuthStore } from "../stores/AuthStore"

export async function getCloudflareUploadUrl(file, customerId, projectId) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/videos/clipurl`, {
            headers: {
                filesize: file.size,
                filename: file.name,
                customer: customerId,
                project: projectId,
                maxduration: 1800,
            },
        })
        let url = await fetched.json()
        return url
    } catch (error) {
        return error.message
    }
}

export async function deleteCloudFlareVideo(uid) {
    logI(`deleteCloudFlareVideo ${uid}`)
    try {
        let fetched = await fetch(`${urlDirectorApi}/videos/delete`, { headers: { uid: uid } })
        let success = await fetched.json()
        return success
    } catch (error) {
        return false
    }
}

// Infos zu allen clips
export async function getCloudFlareVideoInfos() {
    logI("getCloudFlareVideoInfos")

    try {
        let fetched = await fetch(`${urlDirectorApi}/videos/streaminfos`)
        let data = await fetched.json()
        return data
    } catch (error) {
        return error.message
    }
}

// Info zu 1 clip
export async function getCloudFlareVideoInfo(uid) {
    logI("getCloudFlareVideoInfo " + uid)
    try {
        let fetched = await fetch(`${urlDirectorApi}/videos/streaminfos`, { headers: { uid: uid } })
        let data = await fetched.json()
        return data
    } catch (error) {
        return error.message
    }
}

export async function getAllUsers(callerId) {
    try {
        let users
        try {
            let fetched = await fetch(`${urlDirectorApi}/usermanagement/users`, { headers: { caller: callerId } })
            users = await fetched.json()
        } catch (error) {
            users = error.message
        }
        return users
    } catch (error) {
        return error.message
    }
}
export async function addUser(callerId, email, customClaim) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/usermanagement/useradd`, {
            method: "PUT",
            headers: {
                caller: callerId,
                email: email,
                // nur lowercase bei den keys!
                customclaim: customClaim,
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}
export async function getUid(callerId, email) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/usermanagement/useruid`, {
            method: "GET",
            headers: {
                caller: callerId,
                email: email,
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}


export async function setUserProp(callerId, uid, key, data) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/usermanagement/userprop`, {
            method: "PUT",
            headers: {
                caller: callerId,
                uid: uid,
                key: key,
                data: data,
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}

export async function deleteUser(callerId, uid) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/usermanagement/userdelete`, {
            method: "PUT",
            headers: {
                caller: callerId,
                uid: uid,
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}

export async function getNonAdminUsers(userUid) {
    try {
        let users
        try {
            let fetched = await fetch(`${urlDirectorApi}/usermanagement/nonadminusers`, { headers: { caller: userUid } })
            users = await fetched.json()
        } catch (error) {
            users = error.message
        }
        return users
    } catch (error) {
        return error.message
    }
}

export async function setPassword(actionCode, newPassword) {
    let setPwdError = ""
    AuthStore.confirmPasswordReset(actionCode, newPassword)
        .then(async (resp) => {
            logI(resp)
            setPwdSuccess = true
            // und gleich per DA für die dbProd:
            let fetched = await fetch(`${urlDirectorApi}/usermanagement/userpwd`, {
                method: "PUT",
                headers: {
                    email: email,
                    password: newPassword,
                },
            })
            setPwdError = await fetched.json()
        })
        .catch((error) => {
            // Error occurred during confirmation. The code might have expired or the
            // password is too weak.
            setPwdError = error
        })

    return setPwdError
}

export async function getProjectsDict(callerId) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/firestore/projectsdict`, {
            method: "GET",
            headers: {
                caller: callerId,
                db: "lumetourdev",
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}

export async function getClipsDict(callerId) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/firestore/clipsdict`, {
            method: "GET",
            headers: {
                caller: callerId,
                db: "lumetourdev",
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}


export async function setClipMetadata(callerId, uid,customer,project ) {
    try {
        let fetched = await fetch(`${urlDirectorApi}/firestore/clipmetadata`, {
            method: "PUT",
            headers: {
                caller: callerId,
                uid:uid,
                customer:customer,
                project:project
            },
        })
        return await fetched.json()
    } catch (error) {
        return error.message
    }
}









export async function getDownloadUrl(uid) {
    logI("getDownloadUrl " + uid)
    try {
        let fetched = await fetch(`${urlDirectorApi}/videos/streamdownloadurl`, { headers: { uid: uid } })
        let downloadInfo = await fetched.json()
        return downloadInfo
    } catch (error) {
        return error.message
    }
}


