export function getTemplateCardObject(category, itemKey, itemValues) {
    let obj = { category: category, id: itemKey }
    switch (category) {
        case "controls":
            switch (itemKey) {
                case "alternatePosition":
                    obj.title = "Mobile Devices"
                    obj.items = [{ title: "Controls Positioning", radios: ["Bottom", "Center"], selected: itemValues[0] ? "Bottom" : "Center" }]
                    break

                case "autoplay":
                    obj.title = "Toggle autoplay"
                    obj.items = [{ title: "Functionality", radios: ["Enabled", "Disabled"], selected: itemValues[0] ? "Enabled" : "Disabled" }]
                    break
                case "settings":
                    obj.title = "Show settings"
                    obj.items = []
                    obj.items.push({ title: "Functionality", radios: ["Enabled", "Disabled"], selected: itemValues[0] ? "Enabled" : "Disabled" })
                    obj.items.push({ title: "Visibility", radios: ["Visible", "Hidden"], selected: itemValues[1] ? "Visible" : "Hidden" })
                    break
                case "previous":
                    obj.title = "Jump to previous Scene"
                    obj.items = []
                    obj.items.push({ title: "Functionality", radios: ["Enabled", "Disabled"], selected: itemValues[0] ? "Enabled" : "Disabled" })
                    obj.items.push({ title: "Visibility", radios: ["Visible", "Hidden"], selected: itemValues[1] ? "Visible" : "Hidden" })
                    break
                case "next":
                    obj.title = "Jump to next Scene"
                    obj.items = []
                    obj.items.push({ title: "Functionality", radios: ["Enabled", "Disabled"], selected: itemValues[0] ? "Enabled" : "Disabled" })
                    obj.items.push({ title: "Visibility", radios: ["Visible", "Hidden"], selected: itemValues[1] ? "Visible" : "Hidden" })
                    break
                case "backward":
                    obj.title = "Play backward"
                    obj.items = []
                    obj.items.push({ title: "Functionality", radios: ["Enabled", "Disabled"], selected: itemValues[0] ? "Enabled" : "Disabled" })
                    obj.items.push({ title: "Visibility", radios: ["Visible", "Hidden"], selected: itemValues[1] ? "Visible" : "Hidden" })
                    break
            }
            break

        case "splashscreen":
            switch (itemKey) {
                case "textPosition":
                    obj.title = "Screen"
                    obj.items = [{ title: "Text position", radios: ["text-start", "text-center", "text-end"], selected: itemValues[0] }]
                    break
                case "textColor":
                    obj.title = "Text color"
                    obj.items = []
                    obj.items.push({ title: "Branding", radios: ["text-dark", "text-light"], selected: itemValues[0] })
                    obj.items.push({ title: "Content", radios: ["text-dark", "text-light"], selected: itemValues[1] })
                    break
                case "visibility":
                    obj.title = "Visibile elements"
                    obj.items = [
                        {
                            title: "Show on splashscreen",
                            checks: [
                                { id: "Customer", key: "customer", checked: itemValues[0] },
                                { id: "Project", key: "project", checked: itemValues[1] },
                                { id: "Artist", key: "artist", checked: itemValues[2] },
                            ],
                        },
                    ]
                    break
            }
            break
        case "crosslinks":
            obj.title = "Visibility"
            obj.items = [
                {
                    title: "Show crosslinks for ",
                    checks: [
                        { id: "Color change", key: "colorChange", checked: itemValues[0] },
                        { id: "Questions", key: "questions", checked: itemValues[1] },
                    ],
                },
            ]
            break
        case "ui":
            obj.title = "Controls"
            obj.items = [
                {
                    key: "autohide",
                    title: "AutoHide",
                    radios: ["Enabled", "Disabled"],
                    selected: itemValues[0] ? "Enabled" : "Disabled",
                },
            ]

            break
        case "global":
            obj.title = "AutoPause"
            obj.items = [
                {
                    key: "autoPause",
                    title: "When page tab looses focus or browser is minimized",
                    radios: ["Pause entire app", "Pause only video"],
                    selected: itemValues[0] ? "Pause entire app" : "Pause only video",
                },
            ]
            break
    }

    return obj
}
