import { logE, logW, logI, logT } from "./log"
import { getStillAssetsUrl } from "../services/assets"

export function createTrunkData(project, playlistId) {
    let playlistData = []
    if (project) {
        let playlists = project.playlists
        let scenes = project.scenes
        let playlist = playlists.find((o) => o.id == playlistId)
        if (playlist && playlist.scenes) {
            const playlistScenes = playlist.scenes
            let playlistScenesCount = Object.keys(playlistScenes).length
            for (let sc = 0; sc < playlistScenesCount; sc++) {
                let playlistSceneKey = "PlSc-" + sc
                const playlistScene = playlistScenes[playlistSceneKey]
                let trunkScene = scenes.find((o) => o.id == playlistScene.trunk.sceneId)
                if (trunkScene)
                    playlistData.push({
                        playlistSceneKey: playlistSceneKey,
                        playlistSceneNo: sc + 1,
                        sceneName: trunkScene.name,
                        crossLinks: playlistScene.trunk.crossLinks,
                        crossLinkProps: playlistScene.trunk.crossLinkProps,
                        crossLinkPropsSummary: getCrossLinkPropsSummary(playlistScene.trunk.crossLinkProps),
                    })
            }
        }
    }
    return playlistData
}

export function createTrunkSceneData(playlistSceneKey, project, playlistId) {
    let playlistData = []
    if (project) {
        let playlists = project.playlists
        let scenes = project.scenes
        let playlist = playlists.find((o) => o.id == playlistId)

        if (playlist && playlist.scenes) {
            const playlistScenes = playlist.scenes
            const playlistScene = playlistScenes[playlistSceneKey]
            const sceneNo = parseInt(playlistSceneKey.split("-")[1], 10) + 1
            let trunkScene = scenes.find((o) => o.id == playlistScene.trunk.sceneId)
            if (trunkScene)
                playlistData.push({
                    playlistSceneKey: playlistSceneKey,
                    playlistSceneNo: sceneNo,
                    sceneName: trunkScene.name,
                    crossLinks: playlistScene.trunk.crossLinks,
                    crossLinkProps: playlistScene.trunk.crossLinkProps,
                    crossLinkPropsSummary: getCrossLinkPropsSummary(playlistScene.trunk.crossLinkProps),
                })
        }
    }
    return playlistData
}

export function createBranchSceneData(playlistSceneKey, brsqKey, brscKey, project, playlistId) {
    let playlistData = []
    if (project) {
        let playlists = project.playlists
        let scenes = project.scenes
        let playlist = playlists.find((o) => o.id == playlistId)
        if (playlist && playlist.scenes) {
            const playlistScenes = playlist.scenes
            const playlistScene = playlistScenes[playlistSceneKey]
            const sceneNo = parseInt(playlistSceneKey.split("-")[1], 10) + 1
            let branchSequences = playlistScene.branches
            const branchSequence = branchSequences[brsqKey]
            const branchSeqNo = parseInt(brsqKey.split("-")[1], 10) + 1
            const branchScene = branchSequence[brscKey]
            const branchSceneNo = parseInt(brscKey.split("-")[1], 10) + 1
            let scene = scenes.find((o) => o.id == branchScene?.sceneId)
            playlistData.push({
                playlistSceneKey: playlistSceneKey,
                playlistSceneNo: sceneNo,
                branchSequenceKey: brsqKey,
                branchSequenceNo: branchSeqNo,
                branchSceneKey: brscKey,
                branchSceneNo: branchSceneNo,
                branchPk: `${sceneNo}.${branchSeqNo}.${branchSceneNo}`,
                sceneId: scene.id,
                sceneName: scene.name,
                branchEnter: branchScene.branchEnter,
                branchEnd: branchScene.branchEnd,
                branchReturn: branchScene.branchReturn,
                crossLinkProps: branchScene.crossLinkProps,
                crossLinkPropsSummary: getCrossLinkPropsSummary(branchScene.crossLinkProps),
            })
        }
    }
    return playlistData
}

export function getCrossLinkPropsSummary(crossLinkProps) {
    let summary = ""
    if (crossLinkProps) {
        summary = crossLinkProps.identifier
        if (crossLinkProps.label) summary += `, label '${crossLinkProps.label}'`
        if (crossLinkProps.backgroundColor) summary += `, backgroundColor '${crossLinkProps.backgroundColor}'`
    }
    return summary
}

export function createBranchData(project, playlistId) {
    let playlistData = []
    if (project) {
        let playlists = project.playlists
        let scenes = project.scenes
        let playlist = playlists.find((o) => o.id == playlistId)
        if (playlist && playlist.scenes) {
            const playlistScenes = playlist.scenes
            let playlistScenesCount = Object.keys(playlistScenes).length
            for (let sc = 0; sc < playlistScenesCount; sc++) {
                let playlistSceneKey = "PlSc-" + sc
                const playlistScene = playlistScenes[playlistSceneKey]
                let branchSequences = playlistScene.branches
                let branchSequencesCount = Object.keys(branchSequences).length
                for (let brsq = 0; brsq < branchSequencesCount; brsq++) {
                    let brsqKey = "BrSq-" + brsq
                    const branchSequence = branchSequences[brsqKey]
                    let branchScenesCount = Object.keys(branchSequence).length
                    for (let brsc = 0; brsc < branchScenesCount; brsc++) {
                        let brscKey = "BrSc-" + brsc
                        const branchScene = branchSequence[brscKey]
                        let scene = scenes.find((o) => o.id == branchScene?.sceneId)
                        playlistData.push({
                            playlistSceneKey: playlistSceneKey,
                            playlistSceneNo: sc + 1,
                            branchSequenceKey: brsqKey,
                            branchSequenceNo: brsq + 1,
                            branchSceneKey: brscKey,
                            branchSceneNo: brsc + 1,
                            branchPk: `${sc}.${brsq}.${brsc}`,
                            sceneId: scene.id,
                            sceneName: scene.name,
                            branchEnter: branchScene.branchEnter,
                            branchEnd: branchScene.branchEnd,
                            branchReturn: branchScene.branchReturn,
                            crossLinkProps: branchScene.crossLinkProps,
                            crossLinkPropsSummary: getCrossLinkPropsSummary(branchScene.crossLinkProps),
                        })
                    }
                }
            }
        }
    }
    return playlistData
}

function getDuration(scene) {
    if (scene) {
        let clip = scene["clip16x9"]
        if (clip) {
            for (const key in clip) {
                if (clip[key].duration) return clip[key].duration
            }
        }
    }
    return 0
}

export function createPlaylistDiagramData(project, playlistId) {
    if (project) {
        let playlists = project.playlists
        let scenes = project.scenes
        let playlist = playlists.find((o) => o.id == playlistId)
        if (playlist && playlist.scenes) {
            let plName = playlist.name + " " + playlist.language
            let playlistId = playlist.id
            let playlistData = []
            // root = Playlist
            playlistData.push({
                id: playlistId,
                parentId: null,
                name: plName,
                diagId: playlistId,
                fillColor: "#fff",
                border: "#fff" /* fillColor: "#e7704c", border: "#c15433" */,
            })
            const playlistScenes = playlist.scenes
            let playlistScenesCount = Object.keys(playlistScenes).length
            for (let sc = 0; sc < playlistScenesCount; sc++) {
                let playlistSceneKey = "PlSc-" + sc
                const playlistScene = playlistScenes[playlistSceneKey]
                // zuerst trunk scene
                let trunkScene = scenes.find((o) => o.id == playlistScene.trunk.sceneId)
                let trunkSceneData = {
                    // sfDiagram verwendet id + parentId
                    id: playlistSceneKey,
                    parentId: playlistId,
                    trunkIndex: sc,
                    trunkLength: playlistScenesCount,
                    name: trunkScene?.name,
                    sceneId: playlistScene.trunk.sceneId,
                    fillColor: "#FFC107",
                    border: "#d3722e",
                    thumb: getThumbUrl(trunkScene, project),
                    duration: getDuration(trunkScene),
                }
                playlistData.push(trunkSceneData)
                // dann die branch scenes
                let branchSequences = playlistScene.branches
                let branchSequencesCount = Object.keys(branchSequences).length
                for (let brsq = 0; brsq < branchSequencesCount; brsq++) {
                    let branchSequenceKey = "BrSq-" + brsq
                    const branchSequence = branchSequences[branchSequenceKey]
                    let branchScenesCount = Object.keys(branchSequence).length
                    let prevBranchSceneId
                    for (let brsc = 0; brsc < branchScenesCount; brsc++) {
                        let branchSceneKey = "BrSc-" + brsc
                        const branchSequenceScene = branchSequence[branchSceneKey]
                        let branchScene = scenes.find((o) => o.id == branchSequenceScene.sceneId)
                        let branchSceneData = {
                            // sfDiagram verwendet id + parentId
                            id: playlistSceneKey + "." + branchSequenceKey + "." + branchSceneKey,
                            parentId: brsc == 0 ? trunkSceneData.id : prevBranchSceneId,
                            trunkIndex: sc,
                            trunkKey: playlistSceneKey,
                            branchSequenceKey: branchSequenceKey,
                            branchIndex: brsc,
                            branchLength: branchScenesCount,
                            name: branchScene?.name,
                            sceneId: branchSequenceScene.sceneId,
                            fillColor: "#FFC107",
                            border: "#d3722e",
                            thumb: getThumbUrl(branchScene, project),
                            duration: getDuration(branchScene),
                        }
                        playlistData.push(branchSceneData)
                        prevBranchSceneId = branchSceneData.id
                    }
                }
            }
            return playlistData
        }
    }
}
export function getThumbUrl(scene, project) {
    if (scene) {
        let field = scene.stillG3x4
        if (!field) field = scene.stillG9x16
        if (!field) field = scene.stillG16x9
        if (!field) field = scene.stillS3x4
        if (!field) field = scene.stillS9x16
        if (!field) field = scene.stillS16x9
        if (field) return getStillAssetsUrl(project) + field[project.languages[0]] + "?alt=media"
    }
    return ""
}

export function getMaxBranchLength(project, playlistId) {
    let result = 0
    if (project) {
        let playlists = project.playlists
        let playlist = playlists.find((o) => o.id == playlistId)
        if (playlist && playlist.scenes) {
            const playlistScenes = playlist.scenes
            let playlistScenesCount = Object.keys(playlistScenes).length
            for (let sc = 0; sc < playlistScenesCount; sc++) {
                let playlistSceneKey = "PlSc-" + sc
                const playlistScene = playlistScenes[playlistSceneKey]
                let branchSequences = playlistScene.branches
                let branchSequencesCount = Object.keys(branchSequences).length
                for (let brsq = 0; brsq < branchSequencesCount; brsq++) {
                    let brsqKey = "BrSq-" + brsq
                    const branchSequence = branchSequences[brsqKey]
                    if (branchSequence) {
                        let branchScenesCount = Object.keys(branchSequence).length
                        result = Math.max(result, branchScenesCount)
                    } else logW(`branchSequence 'brsqKey' NULL`)
                }
            }
        }
    }
    return result
}
