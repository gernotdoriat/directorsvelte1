import { logE, logW, logI, logT } from "./log"
import { UploadStore } from "../stores/UploadStore"
import { ToastStore } from "../stores/ToastStore"
import * as tus from "tus-js-client"

import { getCloudflareUploadUrl, deleteCloudFlareVideo } from "../services/directorapi"

let testInterval
export async function uploadCloudflareTEST(uploadId, file) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) {
        UploadStore.setUploadInfo(upload.id, "0 %")
        let ci = 0
        testInterval = setInterval(() => {
            ci++
            UploadStore.setUploadInfo(upload.id, ` ${ci * 10}%`)
            if (ci == 10) {
                clearInterval(testInterval)
                let upload = UploadStore.getUpload(uploadId)
                upload.dest.uid = "DUMMY"
                upload.dest.url = "HTTPS://DUMMY/" + upload.id + "/cloudflare"
                UploadStore.setUploadInfo(upload.id, "SUCCESS")
            }
        }, 1000)
    } else logE(uploadId + " NOT FOUND")
}

export async function uploadCloudflare(uploadId, file) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) {
        UploadStore.setUploadInfo(upload.id, "0 %")
        // zuerst ggf. bestehenden upload abhandeln
        if (upload.tus) {
            upload.tus.start()
            return
        }
        // ab hier neuen upload anwerfen
        let url = await getCloudflareUploadUrl(file, upload.dest.customerId, upload.dest.projectId)
        if (url.startsWith("https://upload.videodelivery.net")) {
            // Create a new tus upload
            upload.tus = new tus.Upload(file, {
                uploadUrl: url,
                chunkSize: 5 * 1024 * 1024, // Required
                retryDelays: [0, 3000, 5000, 10000, 20000],
                parallelUploads: 1,
                metadata: {
                    filename: file.name,
                    filetype: file.type,
                },
                onError: function (error) {
                    UploadStore.setUploadInfo(upload.id, error)
                },
                onProgress: function (bytesUploaded, bytesTotal) {
                    let percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2)
                    UploadStore.setUploadInfo(upload.id, `${percentage} %`)
                },
                onSuccess: function () {
                    UploadStore.setUploadInfo(upload.id, "SUCCESS")
                    let uid = getUidFromUrl(upload.tus.url)
                    upload.dest.uid = uid
                    upload.dest.url = getUrl(uid)
                    // und der aktuelle upload.tus hat seinen Dienst geleistet...
                    upload.tus = null
                },
            })
            upload.tus.start()
        } else {
            cancelCloudflareUpload(uploadId)
            ToastStore.append(`Upload cloudflare FAILURE '${url}' `, 4, "danger", "center-top")
        }
    }
}

export function getUrl(uid) {
    return `https://videodelivery.net/${uid}/manifest/video.m3u8`
}

export async function cancelCloudflareUpload(uploadId) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) {
        clearInterval(testInterval)
        UploadStore.setUploadInfo(upload.id, "CANCELED")
        if (upload.tus) {
            //upload.tus.abort()
            let guid = getUidFromUrl(upload.tus.url)
            let result = await deleteCloudFlareVideo(guid)
            logI(`deleteCloudFlareVideo ${guid} -> ${result}`)
            upload.tus = null
        }
    }
}

export function pauseCloudflareUpload(uploadId) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) {
        upload.tus?.abort()
        clearInterval(testInterval)
        UploadStore.setUploadInfo(upload.id, "PAUSE")
    }
}

function getUidFromUrl(url) {
    let parts = url.split("/")
    let guid = parts[parts.length - 1]
    let pos = guid.indexOf("?")
    if (pos) guid = guid.substr(0, pos)
    return guid
}
