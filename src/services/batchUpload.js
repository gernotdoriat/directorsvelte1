import { logE, logW, logI, logT } from "./log"

export function getCloudflareUploads(files, customerId, projectId) {
    let validFiles = []
    for (const file of files) {
        if (file.type && file.type.startsWith("video")) {
            let filepath = file.webkitRelativePath.toLowerCase()
            if (filepath.includes("/clip/")) {
                if (filepath.endsWith(".mp4") || filepath.endsWith(".mkv") || filepath.endsWith(".mov")) {
                    logI("UPLOAD " + file.webkitRelativePath)
                    validFiles.push({ file: file, customerId: customerId, projectId: projectId })
                }
            }
        }
    }
    return validFiles
}

export function getGoogleStorageUploads(files, projectId) {
    let validFiles = []
    for (const file of files) {
        let filepath = file.webkitRelativePath.toLowerCase()
        if (file.type) {
            let filepath = file.webkitRelativePath.toLowerCase()
            if (filepath.includes("/still/")) {
                if (
                    filepath.endsWith(".jpg") ||
                    filepath.endsWith(".jpeg") ||
                    filepath.endsWith(".png") ||
                    filepath.endsWith(".webp") ||
                    filepath.endsWith(".gif")
                ) {
                    logI("UPLOAD " + filepath)
                    validFiles.push({ dir: "still", file: file, projectId: projectId })
                }
                else logW("REJECT " + filepath)
            } else if (filepath.includes("/atmo/")) {
                if (filepath.endsWith(".mp3") || filepath.endsWith(".wav") || filepath.endsWith(".flac")) {
                    logI("UPLOAD " + filepath)
                    validFiles.push({ dir: "atmo", file: file, projectId: projectId })
                }
                else logW("REJECT " + filepath)
            } else if (filepath.includes("/music/")) {
                if (filepath.endsWith(".mp3") || filepath.endsWith(".wav") || filepath.endsWith(".flac")) {
                    logI("UPLOAD " + filepath)
                    validFiles.push({ dir: "music", file: file, projectId: projectId })
                }
                else logW("REJECT " + filepath)
            } else if (filepath.includes("/voice/")) {
                if (filepath.endsWith(".mp3") || filepath.endsWith(".wav") || filepath.endsWith(".flac")) {
                    logI("UPLOAD " + filepath)
                    validFiles.push({ dir: "voice", file: file, projectId: projectId })
                }
                else logW("REJECT " + filepath)
            } else logW("REJECT " + filepath)
        } else logW("REJECT " + filepath)
    }
    return validFiles
}
