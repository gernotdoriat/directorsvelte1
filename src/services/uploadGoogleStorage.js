import { logE, logW, logI, logT } from "./log"
import { UploadStore } from "../stores/UploadStore"
import { ToastStore } from "../stores/ToastStore"
import { AppDataStore } from "../stores/AppDataStore"
import { firebase } from "./firebase"
import { getStorage, ref, uploadBytesResumable, getDownloadURL, listAll } from "firebase/storage"

export async function uploadGoogleStorage(uploadId, file) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) {
        console.log(upload)
        upload.dest.url = file.name
        // Get a non-default bucket from a custom firebase.app.App
        const storage = getStorage(firebase, "gs://lumetour-dev-assets")
        let destPath = `projects/${AppDataStore.getEditProject().id}/${getSubDir(upload.dest.field)}/${upload.dest.url}`
        const storageRef = ref(storage, destPath)
        // Upload the file and metadata
        upload.uploadTask = uploadBytesResumable(storageRef, file)
        // Pause the upload
        //upload.uploadTask.pause()
        // Resume the upload
        //upload.uploadTask.resume()
        // Cancel the upload
        //upload.uploadTask.cancel()
        // Register three observers:
        // 1. 'state_changed' observer, called any time the state changes
        // 2. Error observer, called on failure
        // 3. Completion observer, called on successful completion
        upload.uploadTask.on(
            "state_changed",
            (snapshot) => {
                // Observe state change events such as progress, pause, and resume
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                logI(`${upload.id} ${snapshot.state} ${progress}%`)
                switch (snapshot.state) {
                    case "paused":
                        //logI("Upload is paused")
                        UploadStore.setUploadInfo(upload.id, "PAUSE")
                        break
                    case "running":
                        //logI("Upload is running")
                        UploadStore.setUploadInfo(upload.id, `${progress} %`)
                        break
                }
            },
            (error) => {
                // Handle unsuccessful uploads
                logW(error.message)
                UploadStore.setUploadInfo(upload.id, "CANCELED")
            },
            () => {
                // Handle successful uploads on complete
                UploadStore.setUploadInfo(upload.id, "SUCCESS")
                // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                /*  getDownloadURL(upload.uploadTask.snapshot.ref).then((downloadURL) => {
                    console.log("File available at", downloadURL)
                }) */
            }
        )
    }
}

function getSubDir(field) {
    field = field.toLowerCase()
    if (field.startsWith("still") || field.startsWith("poster") || field.startsWith("branding")) return "still"
    if (field.startsWith("voice")) return "voice"
    return field
}

export async function uploadGoogleStorageCustomerAssets(uploadId, file, customerId) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) {
        console.log(upload)
        upload.dest.url = file.name
        // Get a non-default bucket from a custom firebase.app.App
        const storage = getStorage(firebase, "gs://lumetour-dev-assets")
        let destPath = `customers/${customerId}/${file.name}`
        const storageRef = ref(storage, destPath)
        // Upload the file and metadata
        upload.uploadTask = uploadBytesResumable(storageRef, file)
        upload.uploadTask.on(
            "state_changed",
            (snapshot) => {
                // Observe state change events such as progress, pause, and resume
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                logI(`${upload.id} ${snapshot.state} ${progress}%`)
                switch (snapshot.state) {
                    case "paused":
                        //logI("Upload is paused")
                        UploadStore.setUploadInfo(upload.id, "PAUSE")
                        break
                    case "running":
                        //logI("Upload is running")
                        UploadStore.setUploadInfo(upload.id, `${progress} %`)
                        break
                }
            },
            (error) => {
                // Handle unsuccessful uploads
                logW(error.message)
                UploadStore.setUploadInfo(upload.id, "CANCELED")
            },
            () => {
                // Handle successful uploads on complete
                UploadStore.setUploadInfo(upload.id, "SUCCESS")
                // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                /*  getDownloadURL(upload.uploadTask.snapshot.ref).then((downloadURL) => {
                    console.log("File available at", downloadURL)
                }) */
            }
        )
    }
}

export async function getGoogleStorageAssetInfos(baseDir, idDir, assetSubdir) {
    logI("getGoogleStorageAssetInfos")
    let result = []
    try {
        //const storage = getStorage()
        const storage = getStorage(firebase, "gs://lumetour-dev-assets")

        // Create a reference under which you want to list
        const listRef = ref(storage, `${baseDir}/${idDir}/${assetSubdir}`)

        let res = await listAll(listRef)
        /*    res.prefixes.forEach((folderRef) => {
            // All the prefixes under listRef.
            // You may call listAll() recursively on them.
        }) */
        res.items.forEach((itemRef) => {
            // All the items under listRef.
            result.push({ name: itemRef.name, url: itemRef.fullPath })
        })
    } catch (error) {
        logE(`getGoogleStorageAssetInfos.EXC '${error.message}'`)
    }
    return result
}

export async function getAllGoogleStorageAssetInfos() {
    logI("getAllGoogleStorageAssetInfos")
    let result = []
    try {
        //const storage = getStorage()
        const storage = getStorage(firebase, "gs://lumetour-dev-assets")
        // Create a reference under which you want to list
        const listRef = ref(storage, `projects`)
        let projectDirs = await listAll(listRef)
        for (const projectDir of projectDirs.prefixes) {
            let typeDirs = await listAll(projectDir)
            for (const typeDir of typeDirs.prefixes) {
                let files = await listAll(typeDir)
                for (const file of files.items) {
                    result.push({ name: file.name, url: file.fullPath })
                }
            }
        }
    } catch (error) {
        logE(`getAllGoogleStorageAssetInfos.EXC '${error.message}'`)
    }
    return result
}

export function pauseGoogleStorageUpload(uploadId) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) upload.uploadTask?.pause()
}

export function resumeGoogleStorageUpload(uploadId) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload) upload.uploadTask?.resume()
}

export function cancelGoogleStorageUpload(uploadId) {
    let upload = UploadStore.getUpload(uploadId)
    if (upload && upload.uploadTask) {
        // nötig bei pause, nicht störend bei running
        upload.uploadTask.resume()
        upload.uploadTask.cancel()
    }
}
