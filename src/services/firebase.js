// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCfU0ye9II0YcwIizM-3VqwE5g_99KuSWk",
  authDomain: "lumetour-dev.firebaseapp.com",
  projectId: "lumetour-dev",
  storageBucket: "lumetour-dev.appspot.com",
  messagingSenderId: "572834587071",
  appId: "1:572834587071:web:e8f3c024eda2b3110c810a"
};

// Export firebase instance
export const firebase = initializeApp(firebaseConfig)