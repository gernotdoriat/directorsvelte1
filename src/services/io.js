export function download(object, prefix, name) {
    let jsonStr = JSON.stringify(object)
    let filename = `${prefix}.${name}.json`
    let element = document.createElement("a")
    element.setAttribute("id", prefix + name)
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(jsonStr))
    element.setAttribute("download", filename)
    element.style.display = "none"
    document.body.appendChild(element)
    element.click()
    document.body.removeChild(element)
}
