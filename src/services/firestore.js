import { logE, logW, logI, logT } from "./log"
import { getFirestore, collection, query, where, orderBy, getDocs, getDoc, doc, setDoc, updateDoc, deleteDoc } from "firebase/firestore"
import { firebase } from "./firebase"
import { AuthStore } from "../stores/AuthStore"

const db = getFirestore(firebase)

export let isSupervisor = false
export let isAdmin = false
export let isManager = false
export let projectIds = undefined
export let userEmail = ""

export function getUserType() {
    if (isSupervisor) return "Supervisor"
    if (isAdmin) return "Admin"
    if (isManager) return "Manager"
    return "Worker"
}

export async function getCustomers() {
    isSupervisor = false
    isAdmin = false
    isManager = false
    userEmail = ""
    let customers = {}
    let userData = AuthStore.getUserData()

    // NOCH ziehe ich die Info aus dem CA
    if (userData.customAttributes && userData.customAttributes.admin) isAdmin = true
    if (userData.customAttributes && userData.customAttributes.manage) isManager = true
    userEmail = userData.email
    try {
        const q = query(collection(db, "customers"))
        const querySnapshot = await getDocs(q)
        querySnapshot.forEach((doc) => {
            customers[doc.id] = doc.data()
        })
        isSupervisor = true
    } catch (error) {
        // also user ist KEIN supervisor, aber evtl. ein admin oder manager:
        try {
            // das customersDict aus SEINEN customers bilden
            let userDocData = await getUserDocData(userData.uid)
            projectIds = userDocData.projects
            let customers = userDocData.customers
            if (customers) {
                for (let index = 0; index < customers.length; index++) {
                    const customerId = customers[index]
                    try {
                        const customerDocRef = doc(db, "customers", customerId)
                        const customerDoc = await getDoc(customerDocRef)
                        if (customerDoc.exists()) {
                            customers[doc.id] = doc.data()
                        }
                    } catch (error) {
                        logW(`FAILED to read customer '${customerId}' DOC`)
                    }
                }
            }
        } catch (error) {
            logE(error)
        }
    }
    return customers
}

export async function getCustomerDict() {
    isSupervisor = false
    isAdmin = false
    isManager = false
    userEmail = ""
    let customersDict = []
    let userData = AuthStore.getUserData()

    // NOCH ziehe ich die Info aus dem CA
    if (userData.customAttributes && userData.customAttributes.admin) isAdmin = true
    if (userData.customAttributes && userData.customAttributes.manage) isManager = true
    userEmail = userData.email
    try {
        const q = query(collection(db, "customers"))
        const querySnapshot = await getDocs(q)
        querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots
            let data = doc.data()
            customersDict.push({
                customerId: doc.id,
                customerName: data.customerName,
                customerPoster: data.customerPoster,
                customerBranding: data.customerBranding,
            })
        })
        isSupervisor = true
    } catch (error) {
        // also user ist KEIN supervisor, aber evtl. ein admin oder manager:
        try {
            // das customersDict aus SEINEN customers bilden
            let userDocData = await getUserDocData(userData.uid)
            projectIds = userDocData.projects
            let customers = userDocData.customers
            if (customers) {
                for (let index = 0; index < customers.length; index++) {
                    const customerId = customers[index]
                    try {
                        const customerDocRef = doc(db, "customers", customerId)
                        const customerDoc = await getDoc(customerDocRef)
                        if (customerDoc.exists()) {
                            let customerData = customerDoc.data()
                            customersDict.push({
                                customerId: customerId,
                                customerName: customerData.customerName,
                                customerPoster: customerData.customerPoster,
                                customerBranding: customerData.customerBranding,
                            })
                        }
                    } catch (error) {
                        logW(`FAILED to read customer '${customerId}' DOC`)
                    }
                }
            }
            try {
                // nun noch versuchen, die users collection zu lesen
                const usersq = query(collection(db, "users"))
                await getDocs(usersq)
                // wenn dabei keine EXC auftritt, ist der user ein manager
                //isManager = true
            } catch (error) {
                logI(error)
            }
        } catch (error) {
            logE(error)
        }
    }
    return customersDict
}

export async function getProjects(customerId) {
    let projects = []
    const q = query(collection(db, "projects"), where("customer", "==", customerId), orderBy("name", "asc"))
    const snapshot = await getDocs(q)
    for (let i = 0; i < snapshot.docs.length; i++) {
        const doc = snapshot.docs[i]
        let data = doc.data()
        data["id"] = doc.id
        projects.push(data)
    }
    return projects
}

export async function getProject(projectId) {
    try {
        const docRef = doc(db, "projects", projectId)
        let projectDoc = await getDoc(docRef)
        if (projectDoc.exists()) return projectDoc.data()
        return null
    } catch (error) {
        return null
    }
}

export async function setProject(project) {
    const customerDocRef = doc(db, "projects", project.id)
    const result = await setDoc(customerDocRef, project)
    return result
}

export function getDbUid() {
    return doc(collection(db, "projects")).id
}

export async function getUserDocData(userId) {
    let data
    try {
        const userDocRef = doc(db, "users", userId)
        const userDoc = await getDoc(userDocRef)
        if (userDoc.exists()) data = userDoc.data()
    } catch (error) {
        logE("getUserDocData " + userId + "EXC " + error)
    }
    return data
}

export async function newCustomer(name) {
    const customerDocRef = doc(collection(db, "customers"))
    const result = await setDoc(customerDocRef, { customerName: name })
    return result
}

export async function setCustomerDoc(customerId, customer) {
    const customerDocRef = doc(db, "customers", customerId)
    const result = await setDoc(customerDocRef, customer)
    return result
}

export async function updateCustomerDoc(customerId, key, value) {
    const customerDocRef = doc(db, "customers", customerId)
    let updateObj = {}
    updateObj[key] = value
    const result = await updateDoc(customerDocRef, updateObj)
    return result
}


export async function publishProject(project) {
    let result
    try {
        const publishedDocRef = doc(db, "projects-public", project.id)
        const publishedDoc = await getDoc(publishedDocRef)
        if (publishedDoc.exists()) {
            // bisheriges ggf. retten
            let publishedProject = publishedDoc.data()
            if (project.dataVersion != publishedProject.dataVersion) {
                // tatsächlich was neues, also die zuvor publizierte Version nach projects-public-sav kopieren
                const savDocRef = doc(db, "projects-public-sav", project.id)
                await setDoc(savDocRef, publishedProject)
            } else return "This Version is already published!"
            // und
        }
        // das übergebene Projekt nun publizieren, d.h. unter projects-public speichern
        project.publishedAt = new Date().toString()
        let userData = AuthStore.getUserData()
        project.publishedBy = userData?.email

        result = await setDoc(publishedDocRef, project)
    } catch (error) {
        result = error.message
    }
    return result
}



export async function withdrawPublishing(projectId) {
    let result
    try {
        const savDocRef = doc(db, "projects-public-sav", projectId)
        const savDoc = await getDoc(savDocRef)
        if (savDoc.exists()) {
            // dann den Inhalt des savDocs unter projects-public, d.h. als aktuell publiziertes, speichern
            const publishedDocRef = doc(db, "projects-public", projectId)
            result = await setDoc(publishedDocRef, savDoc.data())
            // und das savDoc entfernen
            await deleteDoc(savDocRef)

        } else {
            const publishedDocRef = doc(db, "projects-public", projectId)
            result = await deleteDoc(publishedDocRef)
        }
    } catch (error) {
        result = error.message
    }
    return result
}

export async function getPublishingInfos(projectId) {
    let result = []
    try {
        const workDocRef = doc(db, "projects", projectId)
        const workDoc = await getDoc(workDocRef)
        if (workDoc.exists()) result.push({ title: "current work", folder: "projects", project: workDoc.data() })
    } catch (error) {
        logE(error.message)
    }

    try {

        const publishedDocRef = doc(db, "projects-public", projectId)
        const publishedDoc = await getDoc(publishedDocRef)
        if (publishedDoc.exists()) result.push({ title: "currently published", folder: "projects-public", project: publishedDoc.data() })
    } catch (error) {
        result = error.message
    }

    try {
        const savDocRef = doc(db, "projects-public-sav", projectId)
        const savDoc = await getDoc(savDocRef)
        if (savDoc.exists()) result.push({ title: "previously published", folder: "projects-public-sav", project: savDoc.data() })
    } catch (error) {
        result = error.message
    }
    return result
}
