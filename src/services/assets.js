export function getFavIconUrl(project) {
    if (project && project.favIcon) {
        return getStillAssetsUrl(project) + project.favIcon + "?alt=media"
    }
    return undefined
}

export function getStillAssetsUrl(project) {
    return `https://firebasestorage.googleapis.com/v0/b/lumetour-dev-assets/o/projects%2F${project.id}%2Fstill%2F`
}

export function resolveField(field, project) {
    if (typeof field == "object") {
        // 1. eingestellte Sprache
        let result = field[project.language]
        // 2. Sprache0
        if (typeof result == "undefined") result = field[project.languages[0]]
        // 3. irgendeine Sprache
        if (typeof result == "undefined") {
            for (const language in field) {
                result = field[language]
                if (typeof result != "undefined") break
            }
        }
        return result
    } else return field
}

export function isAsset(field) {
    if (field) {
        if (field.endsWith("Title")) return false
        if (isVideoAsset(field)) return true
        if (isImageAsset(field)) return true
        if (isAudioAsset(field)) return true
        return false
    }
}
export function isAudioAsset(field) {
    if (field) {
        if (field.endsWith("Title")) return false
        if (field.startsWith("voice")) return true
        if (field == "atmo") return true
        if (field == "music") return true
    }

    return false
}
export function isImageAsset(field) {
    if (field) {
        if (field.endsWith("Title")) return false
        if (field.startsWith("still")) return true
        return false
    }
}
export function isVideoAsset(field) {
    if (field) {
        if (field.endsWith("Title")) return false
        if (field.startsWith("clip")) return true
        return false
    }
}
