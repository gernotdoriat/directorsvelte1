import { logE, logW, logI, logT } from "../services/log"

import { writable } from "svelte/store"
let lastId = 0
const colors = ["primary", "secondary", "success", "danger", "warning", "info", "light", "dark"]
const positions = ["center-top", "center-bottom", "x-y"]

function createToastStore() {
    const { subscribe, update } = writable([], start)

    // Zur Zeit funktionslos, nur fürs Store-Verständnis...
    function start() {
        logI("START ToastStore")
        return stop
    }
    function stop() {
        logI("STOP ToastStore")
        return
    }

    function append(message, timeout, color, position, x, y) {
        let toastId = id()
        update((toasts) => {
            if (!colors.includes(color)) color = "secondary"
            if (!positions.includes(position)) position = "center-top"
            let toast = { id: toastId, message, timeout, color, position, x, y }
            setTimeout(() => {
                removeToast(toast.id)
            }, timeout * 1000)
            return [...toasts, toast]
        })
        return toastId
    }

    function removeToast(toastId) {
        //logI("removeToast " + toastId)
        update((toasts) => {
            return toasts.filter((item) => item.id != toastId)
        })
    }

    function id() {
        if (lastId < 1000) lastId++
        else lastId = 1
        return lastId.toString()
    }

    return {
        subscribe,
        append: (msg, timeout, color, position, x, y) => append(msg, timeout, color, position, x, y),
        remove: (toastId) => removeToast(toastId),
    }
}

export const ToastStore = createToastStore()
