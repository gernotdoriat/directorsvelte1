import { logE, logW, logI, logT } from "../services/log"
import { writable } from "svelte/store"
import { getCustomerDict, getProjects, getDbUid, setProject, getProject, updateCustomerDoc } from "../services/firestore"
import { AuthStore } from "./AuthStore"
export const allCustomers = "All customers"

function createStore() {
    let storeContent = { currentCustomer: { customerName: allCustomers }, projects: [] }
    const { subscribe, update } = writable(storeContent, start)

    function start() {
        logI("START AppDataStore")
        return stop
    }
    function stop() {
        logI("STOP AppDataStore")
        return
    }

    return {
        subscribe,

        initCustomerDict: async () => {
            let customersDict = await getCustomerDict()
            update((store) => {
                store.stamp = new Date()
                store.customersDict = customersDict
                return store
            })
            return customersDict
        },

        setAllProjects: async (customerNames, projectIds) => {
            let projects = []
            for (const customerName of customerNames) {
                if (customerName != allCustomers) {
                    let customer = storeContent.customersDict.find((o) => o.customerName === customerName)
                    if (customer) {
                        let customerProjects = await getProjects(customer.customerId)
                        for (const customerProject of customerProjects) {
                            if (!projectIds || projectIds.includes(customerProject.id)) projects.push(customerProject)
                        }
                    }
                }
            }
            if (projects) {
                update((store) => {
                    store.stamp = new Date()
                    store.currentCustomer = null
                    store.projects = projects
                    return store
                })
            }
        },
        setCurrentCustomer: async (customerName) => {
            let customer = storeContent.customersDict.find((o) => o.customerName === customerName)
            if (customer) {
                // store.projects werden hier mit AKTUELLEN DB Inhalten überschrieben
                let projects = await getProjects(customer.customerId)
                if (projects) {
                    update((store) => {
                        store.stamp = new Date()
                        store.currentCustomer = customer
                        store.projects = projects
                        return store
                    })
                }
            }
        },

        getCurrentCustomerName: () => {
            if (storeContent.currentCustomer) return storeContent.currentCustomer.customerName
            else return allCustomers
        },

        getCustomer: (customerId) => {
            return storeContent.customersDict.find((o) => o.customerId === customerId)
        },

        updateCustomer: async (customerId, key, value) => {
            await updateCustomerDoc(customerId, key, value)
            update((store) => {
                store.stamp = new Date()
                let storeCustomer = store.customersDict.find((o) => o.customerId === customerId)
                if (storeCustomer) storeCustomer[key] = value
                return store
            })
        },

        //#region history
        getEditProject: () => {
            return storeContent.editProject
        },

        setEditProject: (projectId) => {
            let project = storeContent.projects.find((o) => o.id == projectId)
            if (project) {
                // dataVersion sicherstellen
                if (typeof project.dataVersion != "number") project.dataVersion = 1
                let customer = storeContent.customersDict.find((o) => o.customerId === project.customer)
                if (customer) {
                    update((store) => {
                        store.stamp = new Date()
                        store.currentCustomer = customer
                        // KLON des Projekts in der DB
                        store.projectJson = JSON.stringify(project)
                        store.editProject = JSON.parse(JSON.stringify(project))
                        return store
                    })
                    return true
                }
            } else return false
        },
        undoProject: () => {
            let project = storeContent.projects.find((o) => o.id == storeContent.editProject.id)
            if (project) {
                update((store) => {
                    // KLON des Projekts in der DB
                    store.projectJson = JSON.stringify(project)
                    store.editProject = JSON.parse(store.projectJson)
                    store.stamp = new Date()
                    return store
                })
                return true
            }
        },

        uploadProject: async () => {
            let project = storeContent.editProject
            if (project) {
                project.dataVersion++
                project.updatedAt = new Date().toString()
                let userData = AuthStore.getUserData()
                project.updatedBy = userData?.email
                // in firestore speichern
                await setProject(storeContent.editProject)
                // und in der lojkalen Liste
                storeContent.projects[storeContent.editProject.id] = storeContent.editProject
                update((store) => {
                    // KLON des Projekts in der DB
                    store.projectJson = JSON.stringify(project)
                    store.editProject = JSON.parse(store.projectJson)
                    store.stamp = new Date()
                    return store
                })
                return true
            } else return false
        },

        trigger: () => {
            setTimeout(() => {
                logI("")
                logT("TRIGGER AppDataStore")
                logI("")
                update((store) => {
                    return store
                })
            }, 0)
        },
        getCurrentProjectOpenChanges: () => {
            let result = false
            if (storeContent.editProject) {
                logI("ENTER getCurrentProjectOpenChanges")
                result = storeContent.projectJson != JSON.stringify(storeContent.editProject)
                logI("EXIT  getCurrentProjectOpenChanges " + result)
            }
            return result
        },
        //#endregion history

        //#region project

        setProject: (project) => {
            update((store) => {
                store.stamp = new Date()
                store.editProject = project
                return store
            })
        },
        addProject: async (projectObj) => {
            // ADD, d.h. existierendes Doc NICHT überschreiben
            logI("addProject")
            if (!projectObj.id || (await getProject(projectObj.id))) {
                projectObj.id = getDbUid()
            }
            await setProject(projectObj)
            storeContent.projects = await getProjects(storeContent.currentCustomer.customerId)
            update((store) => {
                store.stamp = new Date()
                store = storeContent
                return store
            })
        },

        addScene: () => {
            update((store) => {
                let num = store.editProject.scenes.length + 1
                while (store.editProject.scenes.find((o) => o.name == `scene${num}`)) num++
                store.editProject.scenes.push({ id: getDbUid(), name: `scene${num}` })
                return store
            })
        },
        deleteScene: (sceneId) => {
            update((store) => {
                store.editProject.scenes = store.editProject.scenes.filter((o) => o.id != sceneId)
                return store
            })
        },

        setProjectField: (value, FieldAndLanguage) => {
            let parts = FieldAndLanguage.split("§§")
            if (parts.length == 3) {
                let prop = storeContent.editProject[parts[0]]
                if (!prop) prop = {}
                if (!prop[parts[1]]) prop[parts[1]] = {}
                let field = prop[parts[1]]
                if (!field) field = {}
                field[parts[2]] = value
                update((store) => {
                    store.editProject[parts[0]] = prop
                    return store
                })
            } else if (parts.length == 2) {
                let field = storeContent.editProject[parts[0]]
                if (!field) field = {}
                field[parts[1]] = value
                update((store) => {
                    store.editProject[parts[0]] = field
                    return store
                })
            } else {
                //storeContent.editProject[FieldLanguage]= value
                update((store) => {
                    store.editProject[FieldAndLanguage] = value
                    return store
                })
            }
        },

        setsplashscreenLabels: (splashscreenLabels) => {
            update((store) => {
                store.editProject.splashscreenLabels = splashscreenLabels
                return store
            })
        },
        setLabels: (labelsName, labels) => {
            update((store) => {
                store.editProject[labelsName] = labels
                return store
            })
        },

        //#endregion project

        //#region scenes

        setSceneField: (sceneId, field, value, language) => {
            let scene = storeContent.editProject.scenes.find((o) => o.id == sceneId)
            if (scene) {
                if (language) {
                    if (!scene[field]) scene[field] = {}
                    scene[field][language] = value
                } else scene[field] = value
                //logT(`${scene.name}: ${field} -> ${value} [${language}] `)
                update((store) => {
                    store.editProject = storeContent.editProject
                    store.stamp = new Date()
                    return store
                })
            }
        },

        clearSceneField: (sceneId, field, language) => {
            let scene = storeContent.editProject.scenes.find((o) => o.id == sceneId)
            if (scene) {
                if (language) {
                    if (field.startsWith("clip")) {
                        delete scene[field][language]["url"]
                        delete scene[field][language]["duration"]
                    }
                    delete scene[field][language]
                } else delete scene[field]
                update((store) => {
                    store.editProject = storeContent.editProject
                    store.stamp = new Date()
                    return store
                })
            }
        },

        //#endregion scenes

        //#region playlists
        addPlaylist: () => {
            update((store) => {
                store.stamp = new Date()
                let playlistScenes = {}
                playlistScenes["PlSc-0"] = { trunk: { sceneId: "added" }, branches: {} }
                store.editProject.playlists.push({
                    id: getDbUid(),
                    name: `PLAYLIST${store.editProject.playlists.length + 1}`,
                    scenes: playlistScenes,
                    title: {},
                })
                return store
            })
        },
        clonePlaylist: (id) => {
            let source = storeContent.editProject.playlists.find((o) => o.id == id)
            if (source) {
                let clone = { ...source }
                clone.id = getDbUid()
                update((store) => {
                    store.stamp = new Date()
                    store.editProject.playlists.push(clone)
                    return store
                })
                return true
            } else return false
        },
        deletePlaylist: (id) => {
            update((store) => {
                store.stamp = new Date()
                store.editProject.playlists = store.editProject.playlists.filter((o) => o.id != id)
                return store
            })
        },
        setCurrentPlaylistId: (id) => {
            update((store) => {
                store.stamp = new Date()
                store.currentPlaylistId = id
                return store
            })
        },
        setCurrentSceneIndex: (index) => {
            update((store) => {
                //store.stamp = new Date()
                store.currentSceneIndex = index
                return store
            })
        },
        setCurrentBranchIndex: (index) => {
            update((store) => {
                //store.stamp = new Date()
                store.currentBranchIndex = index
                return store
            })
        },

        setPlaylistTrunkSceneValue: (playlistSceneKey, field, value) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == storeContent.currentPlaylistId)
            let scene = playlist.scenes[playlistSceneKey]
            if (scene) {
                scene.trunk[field] = value
                update((store) => {
                    store.stamp = new Date()
                    store.editProject = storeContent.editProject
                    return store
                })
            }
        },
        setPlaylistTitleValue: (playlistId, language, value) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == playlistId)
            if (playlist) {
                playlist.title[language] = value
                update((store) => {
                    store.stamp = new Date()
                    store.editProject = storeContent.editProject
                    return store
                })
            }
        },
        setPlaylistSceneValue: (playlistId, trunkIndex, branchSequenceKey, branchIndex, field, value) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == playlistId)
            if (playlist) {
                let playlistScene = playlist.scenes[`PlSc-${trunkIndex}`]
                if (playlistScene) {
                    if (branchSequenceKey && typeof branchIndex == "number") {
                        let branchSequence = playlistScene.branches[branchSequenceKey]
                        if (branchSequence) branchSequence[`BrSc-${branchIndex}`][field] = value
                    } else playlistScene.trunk[field] = value
                }
                update((store) => {
                    store.stamp = new Date()
                    store.editProject = storeContent.editProject
                    return store
                })
            }
        },
        indexOfPlaylist: (playlistId) => {
            let index = -1
            let playlist = storeContent.editProject.playlists.find((o) => o.id == playlistId)
            if (playlist) index = storeContent.editProject.playlists.indexOf(playlist)
            return index
        },
        shiftPlaylist: (playlistId, offset) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == playlistId)
            if (playlist) {
                let newPlaylist = [...storeContent.editProject.playlists]
                let index = newPlaylist.indexOf(playlist)
                newPlaylist.splice(index, 1)
                newPlaylist.splice(index + offset, 0, playlist)
                update((store) => {
                    store.stamp = new Date()
                    store.editProject.playlists = newPlaylist
                    return store
                })
            }
        },

        setPlaylistBranchSceneValue: (playlistSceneKey, branchSequenceKey, branchSceneKey, field, value) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == storeContent.currentPlaylistId)
            let scene = playlist.scenes[playlistSceneKey]
            if (scene) {
                let sequence = scene.branches[branchSequenceKey]
                if (sequence) {
                    let scene = sequence[branchSceneKey]
                    if (scene) {
                        scene[field] = value
                        update((store) => {
                            store.stamp = new Date()
                            store.editProject = storeContent.editProject
                            return store
                        })
                    }
                }
            }
        },

        clearCrosslinkProps: (playlistSceneKey, branchSequenceKey, branchSceneKey) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == storeContent.currentPlaylistId)
            let scene = playlist.scenes[playlistSceneKey]
            if (scene) {
                if (branchSequenceKey && branchSceneKey) {
                    let sequence = scene.branches[branchSequenceKey]
                    if (sequence) {
                        let scene = sequence[branchSceneKey]
                        if (scene) {
                            delete scene.crossLinkProps
                            update((store) => {
                                store.stamp = new Date()
                                store.editProject = storeContent.editProject
                                return store
                            })
                        }
                    }
                } else {
                    delete scene.trunk.crossLinkProps
                    update((store) => {
                        store.stamp = new Date()
                        store.editProject = storeContent.editProject
                        return store
                    })
                }
            }
        },

        editDiagramPlaylist: (action, sceneData, args) => {
            let playlist = storeContent.editProject.playlists.find((o) => o.id == storeContent.currentPlaylistId)
            if (playlist) {
                switch (action) {
                    case "changeBranches":
                        if (args) {
                            // ZUERST remove
                            if (args.remove.length > 0) {
                                let playlistScene = playlist.scenes[sceneData.id]
                                for (const removeBranchDescriptor of args.remove) {
                                    let branchNo = removeBranchDescriptor.split(" ")
                                    branchNo = branchNo[branchNo.length - 1]
                                    branchNo = parseInt(branchNo, 10)
                                    delete playlistScene.branches[`BrSq-${branchNo - 1}`]
                                }
                                // dichte playlistScene.branches neu anlegen
                                let newBranches = {}
                                let index = 0
                                for (const key in playlistScene.branches) {
                                    newBranches[`BrSq-${index}`] = playlistScene.branches[key]
                                    index++
                                }
                                playlistScene.branches = newBranches
                            }
                            // DANACH add
                            if (args.add.length > 0) {
                                let playlistScene = playlist.scenes[sceneData.id]
                                let existingBranchesCount = Object.keys(playlistScene.branches).length
                                for (let index = 0; index < args.add.length; index++) {
                                    const add = args.add[index]
                                    let scene = storeContent.editProject.scenes.find((o) => o.name == add)
                                    if (scene) {
                                        // neue branch Sequenz
                                        playlistScene.branches[`BrSq-${existingBranchesCount + index}`] = {}
                                        // mit erster Szene darin
                                        playlistScene.branches[`BrSq-${existingBranchesCount + index}`]["BrSc-0"] = {
                                            sceneId: scene.id,
                                            branchEnter: "atSceneStart",
                                            branchReturn: "toSceneStart",
                                            branchEnd: "toSceneStart",
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case "changeScene":
                        if (args) {
                            // auf .trunkLength und .branchLength prüfen, um nicht über index 0 zu stolpern
                            if (sceneData.trunkLength) {
                                let plsc = playlist.scenes[`PlSc-${sceneData.trunkIndex}`]
                                if (plsc) plsc.trunk.sceneId = args
                            } else if (sceneData.branchLength) {
                                let playlistScene = playlist.scenes[sceneData.trunkKey]
                                let playlistBranchSequence = playlistScene.branches[sceneData.branchSequenceKey]
                                let playlistBranchScene = playlistBranchSequence[`BrSc-${sceneData.branchIndex}`]
                                if (playlistBranchScene) playlistBranchScene.sceneId = args
                            }
                        }
                        break
                    case "remove":
                        if (sceneData.trunkLength) {
                            // TrunkScene: Nachfolger nach vorn kopieren
                            for (let i = sceneData.trunkIndex; i < sceneData.trunkLength - 1; i++) {
                                playlist.scenes[`PlSc-${i}`] = playlist.scenes[`PlSc-${i + 1}`]
                            }
                            // und letze löschen
                            delete playlist.scenes[`PlSc-${sceneData.trunkLength - 1}`]
                        } else if (sceneData.branchLength) {
                            // BranchScene:
                            let playlistScene = playlist.scenes[sceneData.trunkKey]
                            let playlistBranchSequence = playlistScene.branches[sceneData.branchSequenceKey]
                            // auch hier in der BranchSequence  Nachfolger nach vorn kopieren
                            for (let i = sceneData.branchIndex; i < sceneData.branchLength - 1; i++) {
                                playlistBranchSequence[`BrSc-${i}`] = playlistBranchSequence[`BrSc-${i + 1}`]
                            }
                            // und letze löschen
                            delete playlistBranchSequence[`BrSc-${sceneData.branchLength - 1}`]
                            // falls sich damit eine BranchSequence leert, diese selbst entfernen:
                            if (Object.keys(playlistBranchSequence) == 0) {
                                delete playlistScene.branches[sceneData.branchSequenceKey]
                                // dichte playlistScene.branches neu anlegen
                                let newBranches = {}
                                let index = 0
                                for (const key in playlistScene.branches) {
                                    newBranches[`BrSq-${index}`] = playlistScene.branches[key]
                                    index++
                                }
                                playlistScene.branches = newBranches
                            }
                        }
                        break
                    case "addBehind":
                        sceneData.trunkIndex += 1
                    case "addBefore":
                        if (sceneData.trunkLength) {
                            // vom Ende her alle Szenen nach hinten schieben
                            for (let i = sceneData.trunkLength; i > sceneData.trunkIndex; i--) {
                                playlist.scenes[`PlSc-${i}`] = playlist.scenes[`PlSc-${i - 1}`]
                            }
                            // und bei sceneData.trunkIndex eine leere Szene einsetzen
                            playlist.scenes[`PlSc-${sceneData.trunkIndex}`] = { trunk: { sceneId: "added" }, branches: {} }
                            /*  console.log(added) */
                        } else logE(`editDiagramPlaylist '${action}' called for branch scene`)
                        break
                    case "moveRight":
                        sceneData.trunkIndex += 1
                    case "moveLeft":
                        if (sceneData.trunkLength) {
                            if (sceneData.trunkIndex > 0) {
                                let toBeMovedLeft = playlist.scenes[`PlSc-${sceneData.trunkIndex}`]
                                let toBeMovedRight = playlist.scenes[`PlSc-${sceneData.trunkIndex - 1}`]
                                playlist.scenes[`PlSc-${sceneData.trunkIndex - 1}`] = toBeMovedLeft
                                playlist.scenes[`PlSc-${sceneData.trunkIndex}`] = toBeMovedRight
                            } else logE(`editDiagramPlaylist '${action}' called for index 0`)
                        } else logE(`editDiagramPlaylist '${action}' called for branch scene`)
                        break

                    case "addBelow":
                        sceneData.branchIndex += 1
                    case "addAbove":
                        if (sceneData.branchLength) {
                            let playlistScene = playlist.scenes[sceneData.trunkKey]
                            let playlistBranchSequence = playlistScene.branches[sceneData.branchSequenceKey]
                            // vom Ende her alle Szenen nach unten/hinten schieben
                            for (let i = sceneData.branchLength; i > sceneData.branchIndex; i--) {
                                playlistBranchSequence[`BrSc-${i}`] = playlistBranchSequence[`BrSc-${i - 1}`]
                            }
                            // und bei sceneData.branchIndex eine leere Szene einsetzen
                            playlistBranchSequence[`BrSc-${sceneData.branchIndex}`] = {
                                sceneId: "added",
                                branchEnter: "atSceneStart",
                                branchReturn: "toSceneStart",
                                branchEnd: "toSceneStart",
                            }
                        } else logE(`editDiagramPlaylist '${action}' called for trunk scene`)
                        break

                    case "moveDown":
                        sceneData.branchIndex += 1
                    case "moveUp":
                        if (sceneData.branchLength) {
                            if (sceneData.branchIndex > 0) {
                                let playlistScene = playlist.scenes[sceneData.trunkKey]
                                let playlistBranchSequence = playlistScene.branches[sceneData.branchSequenceKey]
                                let toBeMovedUp = playlistBranchSequence[`BrSc-${sceneData.branchIndex}`]
                                let toBeMovedDown = playlistBranchSequence[`BrSc-${sceneData.branchIndex - 1}`]
                                playlistBranchSequence[`BrSc-${sceneData.branchIndex - 1}`] = toBeMovedUp
                                playlistBranchSequence[`BrSc-${sceneData.branchIndex}`] = toBeMovedDown
                            } else logE(`editDiagramPlaylist '${action}' called for index 0`)
                        } else logE(`editDiagramPlaylist '${action}' called for trunk scene`)
                        break

                    default:
                        logE(`editDiagramPlaylist unexpected action '${action}'`)
                        break
                }

                update((store) => {
                    store.stamp = new Date()
                    store.editProject.playlists = storeContent.editProject.playlists
                    return store
                })
            }
        },

        //#endregion playlists

        //#region languages
        setLabel: (languageName, field, value) => {
            let language = storeContent.editProject.labels[languageName]
            if (!language) {
                storeContent.editProject.labels[languageName] = {}
                language = storeContent.editProject.labels[languageName]
            }
            language[field] = value
            update((store) => {
                store.stamp = new Date()
                store.editProject = storeContent.editProject
                return store
            })
        },

        setLanguages: (languages) => {
            storeContent.editProject.languages = languages
            update((store) => {
                store.stamp = new Date()
                store.editProject = storeContent.editProject
                return store
            })
        },

        //#endregion languages

        //#region icons
        setIconValue: (field, value) => {
            if (value) storeContent.editProject.icons[field] = value
            else delete storeContent.editProject.icons[field]
            update((store) => {
                store.stamp = new Date()
                store.editProject = storeContent.editProject
                return store
            })
        },
        //#endregion icons

        //#region template
        updateTemplate: (templateCardObject) => {
            console.log(templateCardObject)
            let category = storeContent.editProject.template[templateCardObject.category]
            switch (templateCardObject.category) {
                case "controls":
                    if (templateCardObject.id == "alternatePosition") category[templateCardObject.id] = templateCardObject.selected == "Bottom" ? true : false
                    else {
                        if (templateCardObject.items.length > 0)
                            category["function"][templateCardObject.id] = templateCardObject.items[0].selected == "Enabled" ? true : false
                        // autoplay hat keine visibility prop
                        if (templateCardObject.items.length > 1)
                            category["visibility"][templateCardObject.id] = templateCardObject.items[1].selected == "Visible" ? true : false
                    }
                    break

                case "splashscreen":
                    if (templateCardObject.id == "visibility") {
                        for (const check of templateCardObject.items[0].checks) {
                            category[templateCardObject.id][check.key] = check.checked
                        }
                    } else category[templateCardObject.id] = templateCardObject.items[0].selected

                    break
                case "crosslinks":
                    for (const check of templateCardObject.items[0].checks) {
                        category["function"][check.key] = check.checked
                    }
                    break
                case "ui":
                    let keyUi = templateCardObject.items[0].key
                    let valueUi = templateCardObject.items[0].selected
                    category[keyUi] = valueUi == "Enabled" ? true : false
                    break
                case "global":
                    let keyG = templateCardObject.items[0].key
                    let valueG = templateCardObject.items[0].selected
                    category[keyG] = valueG == "Pause entire app" ? true : false
                    break
            }

            update((store) => {
                store.stamp = new Date()
                store.editProject = storeContent.editProject
                return store
            })
        },

        //#endregion template
    }
}

export const AppDataStore = createStore()
