import { logE, logW, logI, logT } from "../services/log"
import { writable } from "svelte/store"

function createBatchUploadStore() {
    let counter = 0
    let storeContent = []

    const { subscribe, update } = writable(storeContent, start)

    function start() {
        logI("START BatchUploadStore")
        return stop
    }
    function stop() {
        logI("STOP BatchUploadStore")
        return
    }
    function getUploadId() {
        if (counter > 1000) counter = 0
        counter++
        return "BATCH" + counter
    }

    return {
        subscribe,

        getUploads: () => {
            let result = []
            if (storeContent) for (const upload of storeContent) result.push(upload.id)
            return result
        },

        addUpload: (project, files) => {
            let upload = { id: getUploadId(), project: project, files: files}
            storeContent.push(upload)
            update((store) => {
                store = storeContent
                return store
            })
            return upload.id
        },

        getUpload: (uploadId) => {
            return storeContent.find((o) => o.id == uploadId)
        },

        removeUpload: (uploadId) => {
            storeContent = storeContent.filter((o) => o.id != uploadId)
            update((store) => {
                store = storeContent
                return store
            })
        },

    }
}

export const BatchUploadStore = createBatchUploadStore()
