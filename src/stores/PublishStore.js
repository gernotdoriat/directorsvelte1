import { logE, logW, logI, logT } from "../services/log"
import { writable } from "svelte/store"

import { getPublishingInfos } from "../services/firestore"

function createPublishStore() {
    let storeContent = []

    const { subscribe, update } = writable(storeContent, start)

    function start() {
        logI("START PublishStore")
        return stop
    }
    function stop() {
        logI("STOP PublishStore")
        return
    }

    return {
        subscribe,
        init: async (projectId) => {
            storeContent = await getPublishingInfos(projectId)
            update((store) => {
                store = storeContent
                return store
            })
        },
        getDataVersion: (folder) => {
            let info = storeContent.find((o) => o.folder == folder)
            if (info) return info.project.dataVersion
            return ""
        },
    }
}

export const PublishStore = createPublishStore()
