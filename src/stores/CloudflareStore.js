import { writable } from "svelte/store"
import { logE, logW, logI, logT } from "../services/log"
import { getCloudFlareVideoInfos } from "../services/directorapi"

let storeContent = []
function createCloudflareStore() {
    const { subscribe, set, update } = writable(storeContent, start)

    // Zur Zeit funktionslos, nur fürs Store-Verständnis...
    function start() {
        logI("START CloudflareStore")
        //console.log(storeContent)
        return stop
    }
    function stop() {
        logI("STOP CloudflareStore")
        return
    }

    return {
        subscribe,
        set,
        refresh: async () => {
            storeContent = await getCloudFlareVideoInfos()
            logI(`CloudflareStore.refresh: ${storeContent.length} clips`)
            update((store) => {
                for (const storeImage of store) storeImage.imageOpacity = 0
                return store
            })
        },

        getClipInfos: (customerId) => {
            if (customerId) return storeContent.filter((o) => o.stream_customer == customerId)
            return storeContent
        },
        getClipName: (clipUrl) => {
            if (clipUrl) {
                let clipInfo = storeContent.find((o) => clipUrl.includes(o.stream_uid))
                if (clipInfo) return clipInfo.stream_name
            }
            return clipUrl
        },
    }
}

export const CloudflareStore = createCloudflareStore()
