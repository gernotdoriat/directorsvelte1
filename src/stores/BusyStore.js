import { logE, logW, logI, logT } from "../services/log"
import { writable } from "svelte/store"


//döshföekhföewh

function createBusyStore() {
    let storeContent = { command: "", customer: "", project: "", asset: "" }

    const { subscribe, update } = writable(storeContent, start)

    function start() {
        logI("START BusyStore")
        return stop
    }
    function stop() {
        logI("STOP BusyStore")
        return
    }

    return {
        subscribe,
        init: () => {
            update((store) => {
                storeContent = { command: "", project: "", asset: "" }
                return store
            })
        },
        setCustomer: (customer) => {
            update((store) => {
                store.customer = customer
                return store
            })
        },
        setProject: (project) => {
            update((store) => {
                store.project = project
                return store
            })
        },
        setAsset: (asset) => {
            update((store) => {
                store.asset = asset
                return store
            })
        },
        setCommand: (command) => {
            update((store) => {
                store.command = command
                return store
            })
        },
    }
}

export const BusyStore = createBusyStore()
