import { logE, logW, logI, logT } from "../services/log"
import { writable } from "svelte/store"

function createUploadStore() {
    let counter = 0
    let storeContent = []

    const { subscribe, update } = writable(storeContent, start)

    function start() {
        logI("START UploadStore")
        return stop
    }
    function stop() {
        logI("STOP UploadStore")
        return
    }
    function getUploadId() {
        if (counter > 1000) counter = 0
        counter++
        return "UPLOAD" + counter
    }

    return {
        subscribe,

        getUploads: () => {
            let result = []
            if (storeContent) for (const upload of storeContent) result.push(upload.id)
            // for (let i = 0; i < storeContent?.length; i++) result.push(storeContent[i].id)
            return result
        },

        getUploadByDest: (dest) => {
            let result
            for (let i = 0; i < storeContent?.length; i++) {
                let stDest = storeContent[i].dest
                if (stDest.customerId == dest.customerId && stDest.project == dest.project && stDest.sceneId == dest.sceneId && stDest.field == dest.field) {
                    result = storeContent[i]
                    break
                }
            }
            return result
        },

        addUpload: (source, dest) => {
            let upload = { id: getUploadId(), source: source, dest: dest, info: "" }
            storeContent.push(upload)
            update((store) => {
                store = storeContent
                return store
            })
            return upload.id
        },

        getUpload: (uploadId) => {
            return storeContent.find((o) => o.id == uploadId)
        },

        removeUpload: (uploadId) => {
            logT("removeUpload " + uploadId)
            storeContent = storeContent.filter((o) => o.id != uploadId)
            console.log(storeContent)
            update((store) => {
                store = storeContent
                return store
            })
        },

        setUploadInfo: (uploadId, info) => {
            let upload = storeContent.find((o) => o.id == uploadId)
            if (upload) {
                upload.info = info
                logI(`${upload.id} -> ${upload.info}`)
                update((store) => {
                    store = storeContent
                    return store
                })
            } else logW(`upload ${uploadId} not found`)
        },
        setUploadDuration: (uploadId, duration) => {
            let upload = storeContent.find((o) => o.id == uploadId)
            if (upload) {
                upload.dest.duration = duration
                //logT(`${upload.id} ${upload.info}`)
                update((store) => {
                    store = storeContent
                    return store
                })
            }
        },

        getUploadInfo: (uploadId) => {
            let upload = storeContent.find((o) => o.id == uploadId)
            if (upload) return `${upload.id} ${upload.info}`
        },
    }
}

export const UploadStore = createUploadStore()
